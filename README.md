# Doghouse Promotion Popup

Control a promotion through the customer's cookie.

Example:

    <reference name="before_body_end">
        <block type="promotionpopup/popup" name="promotion_popup">
            <block type="newsletter/subscribe" name="newsletter_subscribe_banner" template="banner.phtml"/>
        </block>
    </reference>

The `promotion_popup` block controls when the banner - which asks for a newsletter
subscription - will be shown.

It has these settings:

- Enable
- Campaign code (for running multiple campaigns)
- Only popup on the homepage
- Number of page view to display popup
- Cookie Lifetime
- Test mode

The promotion can also be triggered by visiting `promotionpopup/trigger/index`. The
controller will redirect to the referrer and show the banner.