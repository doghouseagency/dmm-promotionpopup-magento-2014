<?php
/**
 * Controller that shows the popup regardless of cookie settings
 *
 */
class Doghouse_PromotionPopup_TriggerController extends Mage_Core_Controller_Front_Action {

    public function indexAction() {
        Mage::helper('promotionpopup')->trigger();
        $this->_redirectReferer();
    }

}