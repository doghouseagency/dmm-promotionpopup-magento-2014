<?php

class Doghouse_PromotionPopup_Block_Popup extends Mage_Core_Block_Text_List {

	public function _toHtml() {
		if(Mage::getSingleton('promotionpopup/popup')->shouldDisplay()) {
			return parent::_toHtml();
		}
		return '';
	}

}