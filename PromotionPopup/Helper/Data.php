<?php

class Doghouse_PromotionPopup_Helper_Data extends Mage_Core_Helper_Abstract {

    // Config paths
    const XML_PATH_ENABLED 			= "promotionpopup/general/enabled";
    const XML_PATH_CAMPAIGN_CODE 	= "promotionpopup/general/campaigncode";
    const XML_PATH_HOMEPAGE_ONLY 	= "promotionpopup/general/homepage_only";
    const XML_PATH_PAGEVIEWS 		= "promotionpopup/general/pageviews";
    const XML_PATH_LIFETIME 		= "promotionpopup/general/lifetime";
    const XML_PATH_TESTMODE 		= "promotionpopup/general/testmode";

    // Cookie keys
    const COOKIE_KEY_SEEN 			= "subscribe_popup_shown";
    const COOKIE_KEY_PAGEVIEW_COUNT	= "subscribe_popup_page_views";

    // Config methods
    public function isEnabled() {
        return Mage::getStoreConfig(self::XML_PATH_ENABLED);
    }

    public function getCampaignCode() {
    	return Mage::getStoreConfig(self::XML_PATH_CAMPAIGN_CODE);
    }

    public function getHomepageOnly() {
    	return Mage::getStoreConfig(self::XML_PATH_HOMEPAGE_ONLY);
    }

    public function getPageviewThreshold() {
    	return Mage::getStoreConfig(self::XML_PATH_PAGEVIEWS);
    }

    public function getLifetime() {
    	return Mage::getStoreConfig(self::XML_PATH_LIFETIME);
    }

    public function isTestMode() {
    	return Mage::getStoreConfig(self::XML_PATH_TESTMODE);
    }

    // Cookie methods
    public function getSeenCookieKey() {
    	return self::COOKIE_KEY_SEEN . '_' . $this->getCampaignCode();
    }
}