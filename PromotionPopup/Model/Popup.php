<?php

class Doghouse_PromotionPopup_Model_Popup extends Mage_Core_Model_Abstract {

	protected $_helper;
	protected $_pagecount;
	protected $_seen;

	public function _construct() {
		$this->_helper = Mage::helper('promotionpopup');
		$this->setPagecount($this->readPagecount());
		$this->setSeen($this->readSeen());
	}

	// Public getters/setters
	public function getPagecount() {
		return $this->_pagecount;
	}

	public function setPagecount($val) {
		$this->_pagecount = $val;
		return $this;
	}

	public function getseen() {
		return $this->_seen;
	}

	public function setseen($val) {
		$this->_seen = $val;
		return $this;
	}

	/**
	 * Handles pageviews.
	 * @param  Mage_Core_Controller_Varien_Action $controller
	 * @return self
	 */
	public function handlePageHit(Mage_Core_Controller_Varien_Action $controller) {
		if ($controller->getRequest()->isGet() && !$this->getSeen()) {
			$this->incrementPagecount();
		}
		return $this;
	}

	/**
	 * Increments the pagecount by 1
	 * @return self
	 */
	public function incrementPagecount() {
		$this->setPagecount( $this->getPagecount() + 1 );
		return $this;
	}

	/**
	 * Reads pagecount form cookie
	 * We create a layer because you cant set the cookie and read from it directly in one request
	 * @return int pagecount
	 */
	public function readPagecount() {
		$count = Mage::app()->getCookie()->get(Doghouse_PromotionPopup_Helper_Data::COOKIE_KEY_PAGEVIEW_COUNT);
		if(!$count) {
			$count = 0;
		}
		return $count;
	}

	/**
	 * Writes pagecount to cookie
	 * @return self
	 */
	public function writePagecount() {
		Mage::app()->getCookie()->set(
			Doghouse_PromotionPopup_Helper_Data::COOKIE_KEY_PAGEVIEW_COUNT,
			$this->_pagecount,
			time() + $this->_helper->getLifetime(),
			'/'
		);
		return $this;
	}

	/**
	 * Reads popup seen cookie
	 * @return bool
	 */
	public function readSeen() {
		$seen = Mage::app()->getCookie()->get($this->_helper->getSeenCookieKey());
		if($seen) {
			return 1;
		}
		return 0;
	}

	/**
	 * Writes seen popup value to cookie
	 * @return self
	 */
	public function writeSeen() {
		Mage::app()->getCookie()->set(
			$this->_helper->getSeenCookieKey(),
			$this->_seen,
			time() + $this->_helper->getLifetime(),
			'/'
		);
		return $this;
	}

	/**
	 * Returns whether the popup should be displayed or not
	 * @return bool
	 */
	public function shouldDisplay() {
		if(!$this->_helper->isEnabled()) {
			return false;
		}

        if($this->_helper->isTestMode()) {
            return true;
        }

        if($this->getSeen()) {
        	return false;
        }

        if($this->getPagecount() >= $this->_helper->getPageviewThreshold()) {
        	$this->setPagecount(0);
        	$this->setSeen(1);
            return true;
        }

		return false;
	}

	public function trigger() {

	}


}