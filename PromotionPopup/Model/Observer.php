<?php

class Doghouse_PromotionPopup_Model_Observer
{

	/**
	 * Increments view count in customers cookie
	 * event: controller_action_predispatch
	 */
	public function incrementPagecount($observer)
	{
		//Don't track form posts as these usually redirect and doesn't count as a real pageview
		try {
			//Only if it's a frontend action.
			if (Mage::app()->getRequest()->isGet() && Mage::helper('doghouse_isadmin')->isFrontend()) {
				Mage::getSingleton('promotionpopup/popup')
					->handlePageHit($observer->getControllerAction());
			}
		} catch (Exception $e) {
			Mage::logException($e);
		}
	}

	/**
	 * writes view count to customers cookie
	 * event: controller_action_postdispatch
	 */
	public function writePagecount($observer)
	{
		try {
			//Only if it's a frontend action.
			if (Mage::helper('doghouse_isadmin')->isFrontend()) {
				Mage::getSingleton('promotionpopup/popup')
					->writePagecount()
					->writeSeen();
			}
		} catch (Exception $e) {
			Mage::logException($e);
		}
	}

}